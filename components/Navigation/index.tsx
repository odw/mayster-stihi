import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router'
//import useElveTranslation from '../../lib/useTranslation';
//import { useTranslation } from 'next-i18next'
interface Props {
  className?: string;
}

const Navigation: React.FC<Props> = ({ className }) => {
  //const { t } = useTranslation('common');
  //const {  locale } = useElveTranslation();
  const navClass = className || 'navigation';
  const router = useRouter(); 
  return (
    <nav className={navClass}>
      <ul>
        <li>
          <Link href={`/`} >
            <a>Домой</a>
          </Link>
        </li>
	<li>
          <Link href={`/stihi`}   >
            Стихи
          </Link>
        </li>
        <li>
          <Link href={`/about`}   >
            Обо мне
          </Link>
        </li>
      </ul>
    </nav>
  );
};

    export default Navigation;
