import { NextPage } from 'next';

import Layout from '../../components/Layout';
//import useTranslation from '../lib/useTranslation';
//import useElveTranslation from '../lib/useTranslation';
//import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
//import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
const About: NextPage = () => {
  //const { t, locale } = useTranslation();
  //const { t } = useTranslation('common');
  //const {  locale } = useElveTranslation();
  //const tutorialSlug =
   // locale == 'pt' ? '/post/next-multilingue' : 'en/post/next-intl/';
   const router = useRouter();
  return (
    <Layout title='Стихи' className="about" >
      <section className="page-content">
        <h1>Лишь избранным дано быть на верху</h1>
        {/*<div className="page-text">*/}
        <pre>{`
Ты иногда  в  Житейской  суете
Остановись, пройди  Ленивым взглядом.
Быть  может,  одинокая  Любовь
Тебя  преследует  и  дышит в  спину  Рядом.

А  может, это Первая  Любовь
Осколки  разбросала  ненароком.
И  Светом  отражает  Вновь  и  Вновь
Жизнь  и  Судьбу  предсказанной  Пророком.

А  Ты, идущий  без  оглядки  Напрямик,
Cross-соuntry, по-английски - без Дороги.
И думаешь,  что  не зайдешь в  Судьбы  Тупик.
Уверенный  в  себе, берешь  Пороги. 

Но  иногда, Замедля  быстрый Шаг
На неуверенность и  на  Дуальность Канта,
Крюйс-пеленг посылаешь  Наугад.
В  надежде,  что  придет  на  помощь  Санта.

Но  Жизнь играет  Мною  и  Тобой.
Мы также,  как   Сократ,  найдем Ксантиппу.
И  Все  разрушится  Всевластною  Судьбой.
И  понимаешь: Ты  не  Марк  Агриппа.

Ему,  Марцеллу, баловню  Судьбы,
И  Пантеон  Богам  для  Рима  возводить,
Антония  с  Помпеем  побеждать,
Чтобы  в  веках  быть  Первым и не ныть.

Лишь  Избранным  дано  быть  Наверху.
А  остальным - подручным  материалом.
Признай свой  жребий,  Неба   Правоту.
И  будь  доволен,  Утешайся  Малым.

10.03.2010
	`}</pre>
       {/* </div>*/}
      </section>
    </Layout>
  );
};
//export const getStaticProps = async ({ locale }) => ({
//  props: {
//    ...await serverSideTranslations(locale, ['common']),
//  },
//})
export default About;
