import { NextPage } from 'next';
import Link from 'next/link';
import { useRouter } from 'next/router'
import Layout from '../components/Layout';
//import useTranslation from '../lib/useTranslation';
//import useElveTranslation from '../lib/useTranslation';
//import { useTranslation } from 'next-i18next'
//import { useTranslation } from 'next-i18next'
//import { useTranslation } from 'next-i18next'
//import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
const Home: NextPage = () => {
  //const { t,locale } = useTranslation();
  const router = useRouter();
 // const { t } = useTranslation('common');
  //console.log(t);
  //const {  locale } = useElveTranslation();
  return (
    <Layout title='Домой' className="home">
      <section className="hero">
        <div className="message">
          <h1>Николло Майстер</h1>
          <p>высказывание</p>
          <Link href='/stihi'>
            <a className="button">Стихи</a>
          </Link>
        </div>
      </section>
    </Layout>
  );
};

export default Home

/*
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['common']),
  },
})



/*
export const getStaticProps = async () => ({
  props: {
    namespacesRequired: ['common',],
  }
})
/*
export const getStaticProps = async ({ locale }) => ({
  props: {
    ...await serverSideTranslations(locale, ['common',]),
  },
})

export default withTranslation('common')(Home);
*/
