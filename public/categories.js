export const   post_cats={
    name:[
        {lang:'en',
        value:'/'},
        {lang:'ru',
        value:'/'},
],
    
    type:'cat_dir',
    children:[
        {
            name:[{lang:'en',
            value:'IT'},
            {lang:'ru',
            value:'IT'},
                 ],
                 type:'cat_dir',
                 children:[
                    {
                        name:[{lang:'en',
                        value:'Software'},
                        {lang:'ru',
                        value:'ПО'},
                             ],
                             type:'cat_dir',
                             children:[
                                {
                                    name:[{lang:'en',
                                    value:'Programming languages'},
                                    {lang:'ru',
                                    value:'Языки программирования'},
                                         ],
                                         type:'cat_dir',
                                         children:[
                                             
                                         ]   
                                },
                                {
                                    name:[{lang:'en',
                                    value:'OS'},
                                    {lang:'ru',
                                    value:'Оперционные системы'},
                                         ],
                                         type:'cat_dir',
                                         children:[
                                             
                                         ]   
                                },
                             ]   
                    }, {
                        name:[{lang:'en',
                        value:'Hardware'},
                        {lang:'ru',
                        value:'Железо'},
                             ],
                             type:'cat_dir',
                             children:[
                                 
                             ]   
                    },
                    {
                        name:[{lang:'en',
                        value:'Frontend'},
                        {lang:'ru',
                        value:'Frontend'},
                             ],
                             type:'cat_dir',
                             children:[
                                 
                             ]   
                    }, {
                        name:[{lang:'en',
                        value:'Backend'},
                        {lang:'ru',
                        value:'Backend'},
                             ],
                             type:'cat_dir',
                             children:[
                                 
                             ]   
                    }, {
                        name:[{lang:'en',
                        value:'DevOps'},
                        {lang:'ru',
                        value:'DevOps'},
                             ],
                             type:'cat_dir',
                             children:[
                                 
                             ]   
                    }, {
                        name:[{lang:'en',
                        value:'Security'},
                        {lang:'ru',
                        value:' Безопасность'},
                             ],
                             type:'cat_dir',
                             children:[
                                 
                             ]   
                    },
                    {
                        name:[{lang:'en',
                        value:'ML'},
                        {lang:'ru',
                        value:'Нейронные сети'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    }
                 ]   
        },
        {
            name:[{lang:'en',
            value:'Languages'},
            {lang:'ru',
            value:'Языки'},
                 ],
                 type:'cat_dir',
                 children:[     {
                    name:[{lang:'en',
                    value:'English'},
                    {lang:'ru',
                    value:'Английский'},
                         ],
                         type:'cat_dir',
                         children:[]   
                },
                {
                    name:[{lang:'en',
                    value:'French'},
                    {lang:'ru',
                    value:'Французский'},
                         ],
                         type:'cat_dir',
                         children:[]   
                }, 
                 {
                    name:[{lang:'en',
                    value:'Spanish'},
                    {lang:'ru',
                    value:'Испанский'},
                         ],
                         type:'cat_dir',
                         children:[]   
                },
                {
                    name:[{lang:'en',
                    value:'Dutch(Netherland)'},
                    {lang:'ru',
                    value:'Голландский(Нидерландский)'},
                         ],
                         type:'cat_dir',
                         children:[]   
                },
                {
                    name:[{lang:'en',
                    value:'Swedish'},
                    {lang:'ru',
                    value:'Шведский'},
                         ],
                         type:'cat_dir',
                         children:[]   
                },
                {
                    name:[{lang:'en',
                    value:'Norwegian'},
                    {lang:'ru',
                    value:'Норвежский'},
                         ],
                         type:'cat_dir',
                         children:[]   
                },
                {
                    name:[{lang:'en',
                    value:'Languages misc'},
                    {lang:'ru',
                    value:'Языки разное'},
                         ],
                         type:'cat_dir',
                         children:[]   
                },
            ]   
        },
        {
            name:[{lang:'en',
            value:'Misc'},
            {lang:'ru',
            value:'Разное'},
                 ],
                 type:'cat_dir',
                 children:[
                    {
                        name:[{lang:'en',
                        value:'Medecine'},
                        {lang:'ru',
                        value:'Медицина'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    
                 ]   
        },
        {
            name:[{lang:'en',
            value:'Plans'},
            {lang:'ru',
            value:'Планы'},
                 ],
                 type:'cat_dir',
                 children:[
                    {
                        name:[{lang:'en',
                        value:'1 year plans'},
                        {lang:'ru',
                        value:'План 1 год'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'1st half of the year plans'},
                        {lang:'ru',
                        value:'План 1я половина года'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'2nd half of the year  plans'},
                        {lang:'ru',
                        value:'План 2я половина года'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'Winter plans'},
                        {lang:'ru',
                        value:'План Зима'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'Spring plans'},
                        {lang:'ru',
                        value:'План Весна'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'Summer plans'},
                        {lang:'ru',
                        value:'План Лето'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'Autumn plans'},
                        {lang:'ru',
                        value:'План Осень'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'1 month plans'},
                        {lang:'ru',
                        value:'План 1 месяц'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'1st week plans'},
                        {lang:'ru',
                        value:'План 1я неделя'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'2nd week plans'},
                        {lang:'ru',
                        value:'План 2я неделя'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'3rd week plans'},
                        {lang:'ru',
                        value:'План 3я неделя'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'4th week plans'},
                        {lang:'ru',
                        value:'План 4я неделя'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'1 day plans'},
                        {lang:'ru',
                        value:'План 1 день'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                 ]   
        },
        {
            name:[{lang:'en',
            value:'Reports'},
            {lang:'ru',
            value:'Отчеты'},
                 ],
                 type:'cat_dir',
                 children:[
                    {
                        name:[{lang:'en',
                        value:'1 year report'},
                        {lang:'ru',
                        value:'Отчет 1 год'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'1st half of the year report'},
                        {lang:'ru',
                        value:'Отчет 1я половина года'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'2nd half of the year report'},
                        {lang:'ru',
                        value:'Отчет 2я половина года'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'Winter report'},
                        {lang:'ru',
                        value:'Отчет Зима'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'Spring report'},
                        {lang:'ru',
                        value:'Отчет Весна'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'Summer report'},
                        {lang:'ru',
                        value:'Отчет Лето'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'Autumn report'},
                        {lang:'ru',
                        value:'Отчет Осень'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'1 month report'},
                        {lang:'ru',
                        value:'Отчет 1 месяц'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'1st week report'},
                        {lang:'ru',
                        value:'Отчет 1я неделя'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'2nd week report'},
                        {lang:'ru',
                        value:'Отчет 2я неделя'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'3rd week report'},
                        {lang:'ru',
                        value:'Отчет 3я неделя'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'4th week report'},
                        {lang:'ru',
                        value:'Отчет 4я неделя'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    {
                        name:[{lang:'en',
                        value:'1 day report'},
                        {lang:'ru',
                        value:'Отчет 1 день'},
                             ],
                             type:'cat_dir',
                             children:[]   
                    },
                    
                 ]   
        },
    ]
}