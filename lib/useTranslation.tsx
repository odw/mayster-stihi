import { useContext } from "react";

import { LanguageContext, defaultLocale } from "./LanguageProvider";
//import  {LangStrings } from "../lib/Strings";

export default function useTranslation() {
  const [locale] = useContext(LanguageContext);
/*const LngStrings=LangStrings;
  function t(key: string) {
    if (!LngStrings[locale][key]) {
      console.warn(`No string '${key}' for locale '${locale}'`);
    }

    return LngStrings[locale][key] || LngStrings[defaultLocale][key] || "";
  }
*/
  return {
  // t,
   locale };
}
